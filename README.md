# README #

BoxPress

### What is this repository for? ###

* Imagebox WordPress Framework
* Version 3.0

### How do I get set up? ###

* Install Theme
* Download/Install Advanced Custom Fields
* Go to Advanced Custom Fields 'Field Groups' page, click on 'Sync' link and then Sync all fields.
* Navigate to the boxpress folder in terminal and run 'npm install'

### Contribution guidelines ###

* Talk to Thad

### Who do I talk to? ###

* JD's mom
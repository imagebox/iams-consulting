<?php
/**
 * Displays the page template
 */

?>
<?php get_header(); ?>

	<?php require_once('template-parts/banners/banner--page.php'); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<div class="entry-content">
				<section class="fullwidth-column section">

					<div class="wrap wrap--limited">
				    <div class="column-content">
							<?php while ( have_posts() ) : the_post(); ?>
								<?php get_template_part( 'content', 'page' ); ?>
							<?php endwhile; ?>
				    </div>
				  </div>

				</section>
			</div>

		</main>
	</div>

<?php get_footer(); ?>

<?php 
/**
 * Template Name: Homepage
 *
 * @package BoxPress
 */

$callout_section_title = get_field( 'page_callout_section_title' );

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main homepage">

			<?php if ( ! empty( $callout_section_title )) : ?>

				<div class="callout-block">
					<div class="wrap wrap--full">
						<h1><?php echo $callout_section_title; ?></h1>
					</div>
				</div>

			<?php endif; ?>


			<?php if ( have_rows( 'home_page_callouts' )) : ?>

				<div class="card-grid">

					<?php while ( have_rows( 'home_page_callouts' )) : the_row();
							$title 						= get_sub_field( 'title' );
							$copy							= get_sub_field( 'copy' );
							$link_url 				= get_sub_field( 'link_url' );
							$background_image = get_sub_field( 'background_image' );
						?>

						<div class="card-grid-item">
							<div class="callout-card">
								<a href="<?php echo esc_url( $link_url ); ?>"
										<?php if ( $background_image ) : ?>
											style="background-image: url('<?php echo $background_image['url']; ?>')"
										<?php endif; ?>>

									<?php if ( ! empty( $title )) : ?>
										<div class="callout-card-header">
											<h2><?php echo $title; ?></h2>
										</div>
									<?php endif; ?>

									<?php if ( ! empty( $copy )) : ?>
										<div class="callout-card-body">
											<?php echo $copy; ?>
											<span>Read More</span>
										</div>
									<?php endif; ?>

								</a>
							</div>
						</div>

					<?php endwhile; ?>

				</div>

			<?php endif; ?>

		</main>
	</div>

<?php get_footer(); ?>

<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package BoxPress
 */

get_header(); ?>

	<?php require_once('template-parts/banners/banner--404.php'); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<div class="entry-content">
				<section class="fullwidth-column section">

					<div class="wrap wrap--limited">
						<header class="page-header">
							<h1 class="page-title"><?php _e( 'Sorry, this page can&rsquo;t be found.', 'boxpress' ); ?></h1>
						</header>
				    <div class="column-content">
							<p><?php _e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'boxpress' ); ?></p>

							<?php get_template_part( 'template-parts/search-bar' ); ?>
				    </div>
				  </div>

				</section>
			</div>

		</main>
	</div>

<?php get_footer(); ?>

<?php
/**
 * Displays the Project Card template part
 *
 * @package BoxPress
 */

$project_location_terms = get_the_terms( get_the_ID(), 'project_location' );

?>
<div class="card card--product">
  <a href="<?php the_permalink(); ?>">
    <div class="card-figure">
      <?php if ( has_post_thumbnail() ) : ?>
        <?php the_post_thumbnail( 'project_card' ); ?>
      <?php else : ?>
        <img src="https://via.placeholder.com/387x403"
          width="387"
          height="403"
          alt="">
      <?php endif; ?>
    </div>
    <div class="card-body">
      <h3><?php the_title(); ?></h3>

      <?php if ( $project_location_terms && ! is_wp_error( $project_location_terms )) : ?>
        <p>
          <?php foreach ( $project_location_terms as $term ) : ?>
            <?php echo $term->name; ?>
          <?php endforeach; ?>
        </p>
      <?php endif; ?>
    </div>
    <div class="card-body-button">
      <span class="text-button"><?php _e( 'Read More', 'boxpress' ); ?> <svg width="19" height="19" focusable="false"><use href="#arrow-right-icon" /></svg></span>
    </div>
  </a>
</div>

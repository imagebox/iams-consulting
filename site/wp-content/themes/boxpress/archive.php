<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package BoxPress
 */

get_header(); ?>

	<?php require_once('template-parts/banners/banner--blog.php');?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<section class="blog-page">
				<div class="wrap">
					
					<div class="entry-content">

						<?php if ( have_posts() ) : ?>

							<header class="page-header">
								<?php
									the_archive_title( '<h1 class="page-title">', '</h1>' );
									the_archive_description( '<div class="taxonomy-description">', '</div>' );
								?>
							</header><!-- .page-header -->

							<?php /* Start the Loop */ ?>
							<?php while ( have_posts() ) : the_post(); ?>

								<?php
									/* Include the Post-Format-specific template for the content.
									 * If you want to override this in a child theme, then include a file
									 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
									 */
									get_template_part( 'content', get_post_format() );
								?>

							<?php endwhile; ?>

							<?php boxpress_pagination(); ?>

						<?php else : ?>

							<?php get_template_part( 'content', 'none' ); ?>

						<?php endif; ?>

					</div><!-- .entry-content -->

					<?php get_sidebar(); ?>


				</div><!--.wrap-->
			</section><!--.blog-page-->
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>

<?php

/**
 * BoxPress functions and definitions
 *
 * @package BoxPress
 */

if ( ! function_exists( 'boxpress_setup' )) :
function boxpress_setup() {
	
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails', array( 'post', 'page', 'project' ));
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	));

  add_image_size( 'project_thumb', 1600, 428, true );
  add_image_size( 'project_card', 387, 403, true );
}
endif;
add_action( 'after_setup_theme', 'boxpress_setup' );



/**
 * Enqueue scripts and styles.
 */

function boxpress_scripts() {

  /**
   * Styles
   */

  $style_font_url     = 'https://fonts.googleapis.com/css?family=Montserrat:200,300,400,400i,700';
  $style_screen_path  = get_stylesheet_directory_uri() . '/assets/css/style.min.css';
  $style_screen_ver   = filemtime( get_stylesheet_directory() . '/assets/css/style.min.css' );
  $style_print_path   = get_stylesheet_directory_uri() . '/assets/css/print.css';
  $style_print_ver    = filemtime( get_stylesheet_directory() . '/assets/css/print.css' );

  wp_enqueue_style( 'google-fonts', $style_font_url, array(), false, 'screen' );
  wp_enqueue_style( 'screen', $style_screen_path, array('google-fonts'), $style_screen_ver, 'screen' );
  wp_enqueue_style( 'print', $style_print_path, array( 'screen' ), $style_print_ver, 'print' );


  /**
   * Scripts
   */
  
  $script_modernizr_path  = get_template_directory_uri() . '/assets/js/dev/modernizr.js';
  $script_modernizr_ver   = filemtime( get_template_directory() . '/assets/js/dev/modernizr.js' );
  $script_site_path = get_template_directory_uri() . '/assets/js/build/site.min.js';
  $script_site_ver  = filemtime( get_template_directory() . '/assets/js/build/site.min.js' );

  wp_enqueue_script( 'modernizr', $script_modernizr_path, array(), $script_modernizr_ver, false );
  wp_enqueue_script( 'site', $script_site_path, array( 'jquery' ), $script_site_ver, true );

}
add_action( 'wp_enqueue_scripts', 'boxpress_scripts' );



/**
 * Gravity Forms Settings
 */

// Move GF scripts to footer
add_filter( 'gform_init_scripts_footer', '__return_true' );

// Scroll to confirmation
add_filter( 'gform_confirmation_anchor', '__return_true' );




/**
 * Custom Functions
 */

function query_for_child_page_list() {
  global $wp_query;
  $post       = $wp_query->post;
  $ancestors  = get_post_ancestors( $post );

  if ( empty( $post->post_parent )) {
    $parent = $post->ID;
  } else {
    $parent = end($ancestors);
  }

  // Get list of pages
  return wp_list_pages( array(
    'title_li'  => '',
    'child_of'  => $parent,
    'depth'     => 4,
    'echo'      => false,
  ));
}



/**
 * Numbered Pagination
 */
function boxpress_pagination( $current_query = array() ) {
  $format = '?paged=%#%';

  // Settings for using a main query
  if ( ! $current_query ) {
    global $wp_query;
    $current_query = $wp_query;
    $format = 'page/%#%/';
  }

  $max_num_pages  = $current_query->max_num_pages;
  $found_posts    = $current_query->found_posts;
  $posts_per_page = $current_query->posts_per_page;
  $big        = 999999999;
  $prev_arrow = is_rtl() ? '→' : '←';
  $next_arrow = is_rtl() ? '←' : '→';

  if ( $max_num_pages > 1 && $found_posts > $posts_per_page )  {
    echo '<nav class="pagination" role="navigation" aria-label="Pagination Navigation">';

    echo paginate_links(array(
      'base'      => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
      'format'    => $format,
      'current'   => max( 1, get_query_var( 'paged' )),
      'aria_current' => 'true',
      'total'     => $max_num_pages,
      'mid_size'  => 3,
      'type'      => 'list',
      'prev_text' => $prev_arrow,
      'next_text' => $next_arrow,
    ));

    echo '</nav>';
  }
}


/**
 * Editor Styles
 */

function boxpress_theme_add_editor_styles() {
  add_editor_style( 'custom-editor-style.css' );
}
add_action( 'admin_init', 'boxpress_theme_add_editor_styles' );



/**
 * Menu setup
 */
require get_template_directory() . '/inc/site-navigation.php';

/**
 * Cleanup the header
 */
require get_template_directory() . '/inc/cleanup.php';

/**
 * CPTs
 */
require get_template_directory() . '/inc/cpt/project.php';
require get_template_directory() . '/inc/cpt/cpt-team.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Admin Related Functions
 */
require get_template_directory() . '/inc/admin/default.php';

/**
 * Widgets
 */
require get_template_directory() . '/inc/widgets.php';

/**
 * ACF Options Pages
 */
require get_template_directory() . '/inc/acf-options.php';


			</div>
			
			<footer id="colophon" class="site-footer">
				<div class="primary-footer">
					<div class="wrap wrap--full">

						<div class="footer-col-1">
							<div class="site-info">
								<p>
									<span class="site-info-name"><?php bloginfo( 'name' ); ?></span>
									<span class="site-info-desc"><?php bloginfo( 'description' ); ?></span>
								</p>
							</div>
						</div>

						<div class="footer-col-2">
							<address class="footer-address">
								<?php get_template_part( 'template-parts/address-block' ); ?>
							</address>
						</div>

					</div>
				</div>
			</footer>

		</div>
	</div>
</div>


<?php // GA Tracking Code ?>
<?php the_field( 'tracking_code', 'option' ); ?>

<?php wp_footer(); ?>

</body>
</html>

<?php
/**
 * The template for displaying search results pages.
 *
 * @package BoxPress
 */

get_header(); ?>

	<?php require_once('template-parts/banners/banner--search.php'); ?>


	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="entry-content">
					
				<section class="fullwidth-column section">
					<div class="wrap">
				    <div class="column-content">

							<?php if ( have_posts() ) : ?>

								<header class="page-header">
									<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'boxpress' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
								</header>

								<?php while ( have_posts() ) : the_post(); ?>

									<?php get_template_part( 'content', 'search' ); ?>

								<?php endwhile; ?>

								<?php the_posts_navigation(); ?>

							<?php else : ?>
								<?php get_template_part( 'content', 'none' ); ?>
							<?php endif; ?>

				    </div>
				  </div>
				</section>
			</div>
		</main>
	</div>

<?php get_footer(); ?>

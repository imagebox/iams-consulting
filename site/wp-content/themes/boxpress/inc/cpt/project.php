<?php 

/**
 * BoxPress Projects Custom Post Type
 *
 * @package BoxPress
 */


/**
 * Register Custom Post Type
 */

function cpt_project() {

	$labels = array(
		'name'                  => _x( 'Projects', 'Post Type General Name', 'BoxPress' ),
		'singular_name'         => _x( 'Project', 'Post Type Singular Name', 'BoxPress' ),
		'menu_name'             => __( 'Projects', 'BoxPress' ),
		'name_admin_bar'        => __( 'Project', 'BoxPress' ),
		'parent_item_colon'     => __( 'Parent Project:', 'BoxPress' ),
		'all_items'             => __( 'All Projects', 'BoxPress' ),
		'add_new_item'          => __( 'Add New Project', 'BoxPress' ),
		'add_new'               => __( 'Add New', 'BoxPress' ),
		'new_item'              => __( 'New Project', 'BoxPress' ),
		'edit_item'             => __( 'Edit Project', 'BoxPress' ),
		'update_item'           => __( 'Update Project', 'BoxPress' ),
		'view_item'             => __( 'View Project', 'BoxPress' ),
		'search_items'          => __( 'Search Project', 'BoxPress' ),
		'not_found'             => __( 'Not found', 'BoxPress' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'BoxPress' ),
		'items_list'            => __( 'Projects list', 'BoxPress' ),
		'items_list_navigation' => __( 'Projects list navigation', 'BoxPress' ),
		'filter_items_list'     => __( 'Filter project list', 'BoxPress' ),
	);
	$args = array(
		'label'                 => __( 'Project', 'BoxPress' ),
		'description'           => __( 'Project Custom Post Type with Map', 'BoxPress' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields' ),
		'taxonomies'            => array( 'project_categories', 'project_clients', 'project_awards' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
    'menu_icon'             => 'dashicons-portfolio',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'								=> array( 'slug' => 'projects' ),
		'capability_type'       => 'page',
	);
	register_post_type( 'project', $args );

}
add_action( 'init', 'cpt_project', 0 );



/**
 * Register Custom Taxonomies
 */

function taxonomy_project_building_type() {

  $labels = array(
    'name'                       => _x( 'Building Types', 'Taxonomy General Name', 'BoxPress' ),
    'singular_name'              => _x( 'Building Type', 'Taxonomy Singular Name', 'BoxPress' ),
    'menu_name'                  => __( 'Building Types', 'BoxPress' ),
    'all_items'                  => __( 'All Building Types', 'BoxPress' ),
    'parent_item'                => __( 'Parent Building Type', 'BoxPress' ),
    'parent_item_colon'          => __( 'Parent Building Type:', 'BoxPress' ),
    'new_item_name'              => __( 'New Building Type Name', 'BoxPress' ),
    'add_new_item'               => __( 'Add New Building Type', 'BoxPress' ),
    'edit_item'                  => __( 'Edit Building Type', 'BoxPress' ),
    'update_item'                => __( 'Update Building Type', 'BoxPress' ),
    'view_item'                  => __( 'View Building Type', 'BoxPress' ),
    'separate_items_with_commas' => __( 'Separate Building Types with commas', 'BoxPress' ),
    'add_or_remove_items'        => __( 'Add or remove Building Types', 'BoxPress' ),
    'choose_from_most_used'      => __( 'Choose from the most used', 'BoxPress' ),
    'popular_items'              => __( 'Popular Building Types', 'BoxPress' ),
    'search_items'               => __( 'Search Building Types', 'BoxPress' ),
    'not_found'                  => __( 'Not Found', 'BoxPress' ),
    'items_list'                 => __( 'Building Types list', 'BoxPress' ),
    'items_list_navigation'      => __( 'Building Types list navigation', 'BoxPress' ),
  );
  $args = array(
    'labels'            => $labels,
    'hierarchical'      => true,
    'public'            => true,
    'rewrite'           => array( 'slug' => 'project/building-type' ),
    'show_ui'           => true,
    'show_admin_column' => true,
    'show_in_nav_menus' => true,
    'show_tagcloud'     => false,
  );
  register_taxonomy( 'project_building_type', array( 'project' ), $args );

}
add_action( 'init', 'taxonomy_project_building_type', 0 );

function taxonomy_project_design_category() {

  $labels = array(
    'name'                       => _x( 'Design Categories', 'Taxonomy General Name', 'BoxPress' ),
    'singular_name'              => _x( 'Design Category', 'Taxonomy Singular Name', 'BoxPress' ),
    'menu_name'                  => __( 'Design Categories', 'BoxPress' ),
    'all_items'                  => __( 'All Design Categories', 'BoxPress' ),
    'parent_item'                => __( 'Parent Design Category', 'BoxPress' ),
    'parent_item_colon'          => __( 'Parent Design Category:', 'BoxPress' ),
    'new_item_name'              => __( 'New Design Category Name', 'BoxPress' ),
    'add_new_item'               => __( 'Add New Design Category', 'BoxPress' ),
    'edit_item'                  => __( 'Edit Design Category', 'BoxPress' ),
    'update_item'                => __( 'Update Design Category', 'BoxPress' ),
    'view_item'                  => __( 'View Design Category', 'BoxPress' ),
    'separate_items_with_commas' => __( 'Separate Design Categories with commas', 'BoxPress' ),
    'add_or_remove_items'        => __( 'Add or remove Design Categories', 'BoxPress' ),
    'choose_from_most_used'      => __( 'Choose from the most used', 'BoxPress' ),
    'popular_items'              => __( 'Popular Design Categories', 'BoxPress' ),
    'search_items'               => __( 'Search Design Categories', 'BoxPress' ),
    'not_found'                  => __( 'Not Found', 'BoxPress' ),
    'items_list'                 => __( 'Design Categories list', 'BoxPress' ),
    'items_list_navigation'      => __( 'Design Categories list navigation', 'BoxPress' ),
  );
  $args = array(
    'labels'            => $labels,
    'hierarchical'      => true,
    'public'            => true,
    'rewrite'           => array( 'slug' => 'project/design-category' ),
    'show_ui'           => true,
    'show_admin_column' => true,
    'show_in_nav_menus' => true,
    'show_tagcloud'     => false,
  );
  register_taxonomy( 'project_design_category', array( 'project' ), $args );

}
add_action( 'init', 'taxonomy_project_design_category', 0 );

function taxonomy_project_location() {

  $labels = array(
    'name'                       => _x( 'Locations', 'Taxonomy General Name', 'BoxPress' ),
    'singular_name'              => _x( 'Location', 'Taxonomy Singular Name', 'BoxPress' ),
    'menu_name'                  => __( 'Locations', 'BoxPress' ),
    'all_items'                  => __( 'All Locations', 'BoxPress' ),
    'parent_item'                => __( 'Parent Location', 'BoxPress' ),
    'parent_item_colon'          => __( 'Parent Location:', 'BoxPress' ),
    'new_item_name'              => __( 'New Location Name', 'BoxPress' ),
    'add_new_item'               => __( 'Add New Location', 'BoxPress' ),
    'edit_item'                  => __( 'Edit Location', 'BoxPress' ),
    'update_item'                => __( 'Update Location', 'BoxPress' ),
    'view_item'                  => __( 'View Location', 'BoxPress' ),
    'separate_items_with_commas' => __( 'Separate Location with commas', 'BoxPress' ),
    'add_or_remove_items'        => __( 'Add or remove Location', 'BoxPress' ),
    'choose_from_most_used'      => __( 'Choose from the most used', 'BoxPress' ),
    'popular_items'              => __( 'Popular Locations', 'BoxPress' ),
    'search_items'               => __( 'Search Locations', 'BoxPress' ),
    'not_found'                  => __( 'Not Found', 'BoxPress' ),
    'items_list'                 => __( 'Locations list', 'BoxPress' ),
    'items_list_navigation'      => __( 'Locations list navigation', 'BoxPress' ),
  );
  $args = array(
    'labels'            => $labels,
    'hierarchical'      => true,
    'public'            => false,
    'rewrite'           => false,
    'show_ui'           => true,
    'show_admin_column' => true,
    'show_in_nav_menus' => true,
    'show_tagcloud'     => false,
  );
  register_taxonomy( 'project_location', array( 'project' ), $args );

}
add_action( 'init', 'taxonomy_project_location', 0 );

function taxonomy_project_clients() {

	$labels = array(
		'name'                       => _x( 'Clients', 'Taxonomy General Name', 'BoxPress' ),
		'singular_name'              => _x( 'Client', 'Taxonomy Singular Name', 'BoxPress' ),
		'menu_name'                  => __( 'Clients', 'BoxPress' ),
		'all_items'                  => __( 'All Clients', 'BoxPress' ),
		'parent_item'                => __( 'Parent Client', 'BoxPress' ),
		'parent_item_colon'          => __( 'Parent Client:', 'BoxPress' ),
		'new_item_name'              => __( 'New Client Name', 'BoxPress' ),
		'add_new_item'               => __( 'Add New Client', 'BoxPress' ),
		'edit_item'                  => __( 'Edit Client', 'BoxPress' ),
		'update_item'                => __( 'Update Client', 'BoxPress' ),
		'view_item'                  => __( 'View Client', 'BoxPress' ),
		'separate_items_with_commas' => __( 'Separate clients with commas', 'BoxPress' ),
		'add_or_remove_items'        => __( 'Add or remove clients', 'BoxPress' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'BoxPress' ),
		'popular_items'              => __( 'Popular Clients', 'BoxPress' ),
		'search_items'               => __( 'Search Clients', 'BoxPress' ),
		'not_found'                  => __( 'Not Found', 'BoxPress' ),
		'items_list'                 => __( 'Clients list', 'BoxPress' ),
		'items_list_navigation'      => __( 'Clients list navigation', 'BoxPress' ),
	);
	$args = array(
		'labels'      			=> $labels,
		'hierarchical'			=> true,
		'public' 						=> false,
		'rewrite' 					=> false,
		'show_ui'          	=> true,
		'show_admin_column'	=> true,
		'show_in_nav_menus'	=> true,
		'show_tagcloud'    	=> false,
	);
	register_taxonomy( 'project_clients', array( 'project' ), $args );

}
add_action( 'init', 'taxonomy_project_clients', 0 );


function taxonomy_project_awards() {

	$labels = array(
		'name'                       => _x( 'Awards', 'Taxonomy General Name', 'BoxPress' ),
		'singular_name'              => _x( 'Award', 'Taxonomy Singular Name', 'BoxPress' ),
		'menu_name'                  => __( 'Awards', 'BoxPress' ),
		'all_items'                  => __( 'All Awards', 'BoxPress' ),
		'parent_item'                => __( 'Parent Award', 'BoxPress' ),
		'parent_item_colon'          => __( 'Parent Award:', 'BoxPress' ),
		'new_item_name'              => __( 'New Award Name', 'BoxPress' ),
		'add_new_item'               => __( 'Add New Award', 'BoxPress' ),
		'edit_item'                  => __( 'Edit Award', 'BoxPress' ),
		'update_item'                => __( 'Update Award', 'BoxPress' ),
		'view_item'                  => __( 'View Award', 'BoxPress' ),
		'separate_items_with_commas' => __( 'Separate awards with commas', 'BoxPress' ),
		'add_or_remove_items'        => __( 'Add or remove awards', 'BoxPress' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'BoxPress' ),
		'popular_items'              => __( 'Popular Awards', 'BoxPress' ),
		'search_items'               => __( 'Search Awards', 'BoxPress' ),
		'not_found'                  => __( 'Not Found', 'BoxPress' ),
		'items_list'                 => __( 'Awards list', 'BoxPress' ),
		'items_list_navigation'      => __( 'Awards list navigation', 'BoxPress' ),
	);
	$args = array(
		'labels'           	=> $labels,
		'hierarchical'     	=> true,
		'public' 						=> false,
		'rewrite'						=> false,
		'show_ui'          	=> true,
		'show_admin_column'	=> true,
		'show_in_nav_menus'	=> true,
		'show_tagcloud'    	=> false,
	);
	register_taxonomy( 'project_awards', array( 'project' ), $args );

}
add_action( 'init', 'taxonomy_project_awards', 0 );

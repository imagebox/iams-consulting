<?php 

/**
 * BoxPress Locations Custom Post Type
 *
 * @package BoxPress
 */


/**
 * Register Custom Post Type
 */

function cpt_locations() {

	$labels = array(
		'name'                  => _x( 'Locations', 'Post Type General Name', 'BoxPress' ),
		'singular_name'         => _x( 'Location', 'Post Type Singular Name', 'BoxPress' ),
		'menu_name'             => __( 'Locations', 'BoxPress' ),
		'name_admin_bar'        => __( 'Location', 'BoxPress' ),
		'parent_item_colon'     => __( 'Parent Location:', 'BoxPress' ),
		'all_items'             => __( 'All Locations', 'BoxPress' ),
		'add_new_item'          => __( 'Add New Location', 'BoxPress' ),
		'add_new'               => __( 'Add New', 'BoxPress' ),
		'new_item'              => __( 'New Location', 'BoxPress' ),
		'edit_item'             => __( 'Edit Location', 'BoxPress' ),
		'update_item'           => __( 'Update Location', 'BoxPress' ),
		'view_item'             => __( 'View Location', 'BoxPress' ),
		'search_items'          => __( 'Search Location', 'BoxPress' ),
		'not_found'             => __( 'Not found', 'BoxPress' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'BoxPress' ),
		'items_list'            => __( 'Locations list', 'BoxPress' ),
		'items_list_navigation' => __( 'Locations list navigation', 'BoxPress' ),
		'filter_items_list'     => __( 'Filter locations list', 'BoxPress' ),
	);
	$args = array(
		'label'                 => __( 'Location', 'BoxPress' ),
		'description'           => __( 'Location Custom Post Type with Map', 'BoxPress' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields' ),
		'taxonomies'            => array( 'location_categories' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-location-alt',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'locations', $args );

}
add_action( 'init', 'cpt_locations', 0 );



/**
 * Register Custom Taxonomy
 */
function taxonomy_location_categories() {

	$labels = array(
		'name'                       => _x( 'Categories', 'Taxonomy General Name', 'BoxPress' ),
		'singular_name'              => _x( 'Category', 'Taxonomy Singular Name', 'BoxPress' ),
		'menu_name'                  => __( 'Categories', 'BoxPress' ),
		'all_items'                  => __( 'All Categories', 'BoxPress' ),
		'parent_item'                => __( 'Parent Category', 'BoxPress' ),
		'parent_item_colon'          => __( 'Parent Category:', 'BoxPress' ),
		'new_item_name'              => __( 'New Category Name', 'BoxPress' ),
		'add_new_item'               => __( 'Add New Category', 'BoxPress' ),
		'edit_item'                  => __( 'Edit Category', 'BoxPress' ),
		'update_item'                => __( 'Update Category', 'BoxPress' ),
		'view_item'                  => __( 'View Category', 'BoxPress' ),
		'separate_items_with_commas' => __( 'Separate categories with commas', 'BoxPress' ),
		'add_or_remove_items'        => __( 'Add or remove categories', 'BoxPress' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'BoxPress' ),
		'popular_items'              => __( 'Popular Categories', 'BoxPress' ),
		'search_items'               => __( 'Search Categories', 'BoxPress' ),
		'not_found'                  => __( 'Not Found', 'BoxPress' ),
		'items_list'                 => __( 'Categories list', 'BoxPress' ),
		'items_list_navigation'      => __( 'Categories list navigation', 'BoxPress' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
	);
	register_taxonomy( 'location_categories', array( 'locations' ), $args );

}
add_action( 'init', 'taxonomy_location_categories', 0 );

<?php

/**
 * Number of dashboard icons
 */

function wpse126301_dashboard_columns() {
	add_screen_option(
		'layout_columns',
		array(
			'max'     => 2,
			'default' => 1
		)
	);
}
add_action( 'admin_head-index.php', 'wpse126301_dashboard_columns' );



/**
 * Custom Dashboard Widget
 */

add_action('wp_dashboard_setup', 'my_custom_dashboard_widgets');
 
function my_custom_dashboard_widgets() {
	global $wp_meta_boxes;
	wp_add_dashboard_widget('custom_help_widget', 'Welcome', 'custom_dashboard_help');
}

function custom_dashboard_help() {
	
	require get_template_directory() . '/inc/admin/dashboard-widget.php';

	//echo '<p>Welcome to Custom Blog Theme! Need help? Contact the developer <a href="mailto:yourusername@gmail.com">here</a>. For WordPress Tutorials visit: <a href="http://www.wpbeginner.com" target="_blank">WPBeginner</a></p>';
}



/**
 * BoxPress Logo
 */

function my_login_logo() { ?>
	<style type="text/css">
		body.login div#login h1 a {
			background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/img/default/boxpress.png);
			padding-bottom: 30px;
			height: 40px;
			width: 140px;
			background-size: 140px, 40px;
		}
	</style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );



/**
 * Show the kitchen sink by default
 */

function unhide_kitchensink( $args ) {
	$args['wordpress_adv_hidden'] = false;
		return $args;
}
add_filter( 'tiny_mce_before_init', 'unhide_kitchensink' );



/**
 * Set default media link to 'none'
 */

update_option('image_default_link_type','none');



/**
 * Set default gallery link to 'file'
 */

function my_gallery_default_type_set_link( $settings ) {
	$settings['galleryDefaults']['link'] = 'file';
	return $settings;
}
add_filter( 'media_view_settings', 'my_gallery_default_type_set_link');




/**
 * Remove 'Editor' from Appearance menu
 */
function remove_editor_menu() {
  remove_action('admin_menu', '_add_themes_utility_last', 101);
}
add_action('_admin_menu', 'remove_editor_menu', 1);





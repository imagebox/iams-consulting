<?php
/**
 * Displays the Blog Archive / Post banner
 *
 * @package BoxPress
 */
?>
<header class="banner">
  <div class="banner-body">
    <div class="wrap">
      <span class="banner-title h1"><?php _e('Blog', 'boxpress'); ?></span>
    </div>
  </div>
  <img class="banner-bkg" src="<?php bloginfo('template_directory'); ?>/assets/img/global/banners/default-banner.jpg"
    width="1600"
    height="214"
    alt="">
</header>

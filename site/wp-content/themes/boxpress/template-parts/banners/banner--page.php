<?php
/**
 * Displays the page banner. Defaults to image set in the options page.
 */

if ( ! is_front_page() ) : ?>

	<header class="banner">
    <div class="banner-body">
      <div class="wrap">
  			<span class="banner-title h1"><?php
          if ( 0 == $post->post_parent ) {
  					the_title();
          } else {
  					$parents = get_post_ancestors( $post->ID );
  					echo apply_filters( "the_title", get_the_title( end ( $parents ) ) );
          } 
  			?></span>
      </div>
    </div>

    <?php // Default to post thumbnail ?>
    <?php if ( ! has_post_thumbnail() ) : ?>
      <?php if ( 0 == $post->post_parent ) : ?>

        <img class="banner-bkg" src="<?php bloginfo('template_directory'); ?>/assets/img/global/banners/default-banner.jpg"
          width="1600"
          height="214"
          alt="">

      <?php else :
          $parents = get_post_ancestors( $post->ID );
          $featured_image = get_the_post_thumbnail( end ( $parents ));
        ?>

        <?php if ( ! empty( $featured_image )) : ?>
          <?php echo $featured_image; ?>
        <?php else : ?>

          <img class="banner-bkg" src="<?php bloginfo('template_directory'); ?>/assets/img/global/banners/default-banner.jpg"
            width="1600"
            height="214"
            alt="">

        <?php endif; ?>
      <?php endif; ?>

    <?php else : ?>
      <?php the_post_thumbnail( 'post-thumbnail', array( 'class' => 'banner-bkg' ) ); ?>
    <?php endif; ?>
	</header>

<?php endif; ?>

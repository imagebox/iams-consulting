<?php
/**
 * Displays the Blog Archive / Post banner
 *
 * @package BoxPress
 */

$term = array();
$banner_image = get_field( 'project_banner_background', 'option' );

if ( is_tax() ) {
  $term = get_queried_object();

  if ( $term ) {
    $term_banner_image = get_field( 'project_cat_banner_image', $term );

    if ( $term_banner_image ) {
      $banner_image = $term_banner_image;
    }
  }
}

?>
<header class="banner">
  <div class="banner-body">
    <div class="wrap">
      <span class="banner-title h1"><?php _e( 'Portfolio', 'boxpress' ); ?></span>
    </div>
  </div>
  <?php if ( $banner_image ) : ?>
    <img class="banner-bkg" draggable="false"
      src="<?php echo $banner_image['url']; ?>"
      width="<?php echo $banner_image['width']; ?>"
      height="<?php echo $banner_image['height']; ?>"
      alt="">
  <?php endif; ?>
</header>

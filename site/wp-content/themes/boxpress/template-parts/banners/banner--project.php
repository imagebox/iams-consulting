<?php
/**
 * Displays the Blog Archive / Post banner
 *
 * @package BoxPress
 */

$banner_image = get_field( 'project_banner_background', 'option' );

?>
<header class="banner banner--project">
  <div class="banner-body">
    <div class="wrap">
      <!-- <span class="vh banner-title h1"><?php _e( 'Projects', 'boxpress' ); ?></span> -->
    </div>
  </div>
  <?php if ( has_post_thumbnail() ) : ?>
    <?php the_post_thumbnail( 'project_thumb', array( 'class' => 'banner-bkg' ) ); ?>
  <?php elseif ( $banner_image ) : ?>
    <img class="banner-bkg" draggable="false"
      src="<?php echo $banner_image['url']; ?>"
      width="<?php echo $banner_image['width']; ?>"
      height="<?php echo $banner_image['height']; ?>"
      alt="">
  <?php else : ?>
    <img class="banner-bkg" src="<?php bloginfo('template_directory'); ?>/assets/img/global/banners/default-banner.jpg"
      width="1600"
      height="214"
      alt="">
  <?php endif; ?>
</header>

<div class="mobile-head">

  <a class="mobile-branding" href="<?php echo esc_url( home_url( '/' )); ?>" rel="home">
    <span class="vh"><?php bloginfo('name'); ?></span>
    <svg class="site-logo" width="44" height="44" focusable="false">
      <use href="#site-logo" />
    </svg>
  </a>

	<a class="icon-menu toggle-nav" href="#">
		<span class="vh">Menu</span>
    <svg class="menu-icon-svg" width="26" height="16" focusable="false">
      <use href="#menu-icon" />
    </svg>
	</a>
</div>

<nav id="mobile-nav" class="mobile-nav">

  <div class="mobile-nav-controls">

    <a class="mobile-nav-branding" href="<?php echo esc_url( home_url( '/' )); ?>" rel="home">
      <span class="vh"><?php bloginfo('name'); ?></span>
      <svg width="44" height="44" class="site-logo" viewBox="0 0 59 59"><title>Iams Consulting</title><g fill="none" fill-rule="evenodd"><path fill="currentColor" d="M11.685 32.804h33.472V27.63H11.685z"/><path d="M58.7 30.217a4.515 4.515 0 1 1-9.03-.001 4.515 4.515 0 0 1 9.03 0" fill="currentColor"/><path d="M46.601 43.344c-4.109 5.344-10.563 8.79-17.825 8.79-12.415 0-22.478-10.063-22.478-22.478 0-12.414 10.063-22.478 22.478-22.478 7.612 0 14.336 3.788 18.402 9.578l5.477-3.161C47.485 5.925 38.72.88 28.775.88 12.885.88 0 13.764 0 29.656 0 45.55 12.884 58.432 28.776 58.432c9.595 0 18.087-4.7 23.315-11.92l-5.49-3.168z" fill="currentColor"/></g></svg>
    </a>

    <a class="mobile-nav-toggle toggle-nav" href="#">
      <span class="vh">Close</span>
      <svg class="menu-icon-svg" width="26" height="16" focusable="false">
        <use href="#close-icon" />
      </svg>
    </a>
  </div>

	<ul class="mobile-main-nav">
		<?php
			wp_nav_menu( array(
		    'theme_location' 	=> 'primary',
		    'items_wrap' 			=> '%3$s',
		    'container' 			=> false,
		  ));
	  ?>
	</ul>
</nav>

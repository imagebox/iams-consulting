<?php
/**
 * Team Card
 */

$headshot = get_field( 'headshot' );
$job_title = get_field( 'job_title' );
$email_address = get_field( 'email_address' );
$linkedin_profile_url = get_field( 'linkedin_profile_url' );

?>
<!--
<button type="button" class="card-team-member"
  data-micromodal-trigger="team-member-<?php echo get_the_ID(); ?>">
-->
<div class="card-team-member" style="cursor:default;">
  <?php if ( $headshot ) : ?>
    <img class="card-team-member-featured-image" src="<?php echo esc_url( $headshot['url'] ); ?>"
      width="<?php echo $headshot['width']; ?>"
      height="<?php echo $headshot['height']; ?>"
      alt="">
  <?php endif; ?>
  <div class="card-team-member-body">
    <h4 class="card-team-title"><?php the_title(); ?></h4>
    <?php if ( $job_title ) : ?>
      <p class="card-team-job-title"><?php echo $job_title; ?></p>
    <?php endif; ?>
  </div>
</div>

<!--
<div class="modal modal--team-member" id="team-member-<?php echo get_the_ID(); ?>" aria-hidden="true">
  <div class="modal-overlay" tabindex="-1" data-micromodal-close>
    <div class="modal-container" role="dialog" aria-modal="true" aria-labelledby="modal-<?php echo get_the_ID(); ?>-title">
      <button class="modal-close-btn" aria-label="Close modal" data-micromodal-close>
        <span class="vh"><?php _e('Close', 'boxpress'); ?></span>
      </button>

      <div class="l-team-pop">
        <div class="l-team-pop-sidebar">
          <?php if ( $headshot ) : ?>
            <img class="team-sidebar-headshot" src="<?php echo $headshot['url']; ?>"
              width="<?php echo $headshot['width']; ?>"
              height="<?php echo $headshot['height']; ?>"
              alt="">
          <?php endif; ?>

          <?php if ( have_rows( 'certifications' )) : ?>
            <div class="team-sidebar-block">
              <h3 class="team-sidebar-title"><?php _e( 'Certifications', 'boxpress' ); ?></h3>
              <ul class="team-sidebar-list">
                <?php while ( have_rows( 'certifications' )) : the_row(); ?>
                  <?php
                    $certification = get_sub_field( 'certification' );
                  ?>
                  <?php if ( ! empty( $certification )) : ?>
                    <li><?php echo $certification; ?></li>
                  <?php endif; ?>
                <?php endwhile; ?>
              </ul>
            </div>
          <?php endif; ?>

          <?php if ( ! empty( $email_address ) || ! empty( $linkedin_profile_url )) : ?>
            <div class="team-sidebar-block">
              <h3 class="team-sidebar-title"><?php _e( 'Connect', 'boxpress' ); ?></h3>
              <ul class="team-social-list">
                <?php if ( ! empty( $email_address )) : ?>
                  <li>
                    <a href="mailto:<?php echo $email_address; ?>">
                      <svg class="email-icon" width="33" height="33" focusable="false">
                        <use href="#email-icon" />
                      </svg>
                    </a>
                  </li>
                <?php endif; ?>
                <?php if ( ! empty( $linkedin_profile_url )) : ?>
                  <li>
                    <a href="<?php echo esc_url( $linkedin_profile_url ); ?>" target="_blank">
                      <svg class="linkedin-icon" width="33" height="33" focusable="false">
                        <use href="#social-linkedin" />
                      </svg>
                    </a>
                  </li>
                <?php endif; ?>
              </ul>
            </div>
          <?php endif; ?>
        </div>
        <div class="l-team-pop-main">
          <div>
            <h2 id="modal-<?php echo get_the_ID(); ?>-title" class="team-content-title"><?php the_title(); ?></h2>
            <?php if ( $job_title ) : ?>
              <p class="team-popup-job-title"><?php echo $job_title; ?></p>
            <?php endif; ?>
            <?php the_content(); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
-->
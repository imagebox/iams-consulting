<?php
/**
 * Displays the formatted address
 *
 * @package BoxPress
 */
?>
<div itemscope itemtype="http://schema.org/LocalBusiness">

  <?php if ( have_rows('site_offices', 'option') ) : ?>
    <?php while ( have_rows('site_offices', 'option') ) : the_row();
        $street_address = get_sub_field('street_address');
        $address_line_2 = get_sub_field('address_line_2');
        $city           = get_sub_field('city');
        $state          = get_sub_field('state');
        $zip            = get_sub_field('zip');
        $country        = get_sub_field('country');
        $phone_number   = get_sub_field('phone_number');
        $email          = get_sub_field('email');
      ?>
      
      <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">

        <div class="footer-address-postal">
          <p>
            <span class="vh" itemprop="name"><?php bloginfo( 'name' ); ?></span>

            <?php if ( ! empty( $street_address )) : ?>

              <span itemprop="streetAddress">
                <?php echo $street_address; ?>

                <?php if ( ! empty( $address_line_2 )) : ?>
                  <?php echo $address_line_2; ?>
                <?php endif; ?>
              </span>

            <?php endif; ?>

            <?php if ( ! empty( $city )) : ?>
              <span itemprop="addressLocality"><?php echo $city ?></span>, 
            <?php endif; ?>

            <?php if ( ! empty( $state )) : ?>
              <span itemprop="addressRegion"><?php echo $state; ?></span> 
            <?php endif; ?>

            <?php if ( ! empty( $zip )) : ?>
              <span itemprop="postalCode"><?php echo $zip; ?></span> 
            <?php endif; ?>

            <?php if ( ! empty( $country )) : ?>
              <span class="vh" itemprop="addressCountry"><?php echo $country; ?></span>
            <?php endif; ?>
          </p>
        </div>

        <?php if ( ! empty( $phone_number ) ||
                   ! empty( $email )) : ?>

          <div class="footer-address-contact">
            <p>

              <?php if ( ! empty( $phone_number )) : ?>
                <span class="vh">Phone: </span>
                <a href="tel:<?php echo $phone_number; ?>" itemprop="telephone">
                  <?php echo $phone_number; ?>
                </a>
              <?php endif; ?>
              
              <?php if ( ! empty( $email )) : ?>
                <span class="vh">Email: </span>
                <a class="email" href="mailto:<?php echo $email; ?>" itemprop="email">
                  <span class="vh"><?php echo $email; ?></span>
                  <svg class="email-icon" width="33" height="33" focusable="false">
                    <use href="#email-icon" />
                  </svg>
                </a>
              <?php endif; ?>

            </p>
          </div>

        <?php endif; ?>

      </div>

    <?php endwhile; ?>
  <?php endif; ?>

</div>

<?php
/**
 * Displays the Social Navigation
 *
 * @package BoxPress
 */

$social_fb = get_field( 'facebook', 'option' );
$social_tw = get_field( 'twitter_username', 'option' );
$social_gp = get_field( 'google_plus', 'option' );
$social_yt = get_field( 'youtube', 'option' );
?>

<h3><?php _e( 'Connect with Us', 'boxpress' ); ?></h3>

<ul class="social-nav">
  <?php if ( ! empty( $social_fb )) : ?>
    <li>
      <a target="_blank" href="<?php echo $social_fb; ?>">
        <svg class="social-fb-svg" width="31" height="31" focusable="false">
          <use href="#social-fb"/>
        </svg>
      </a>
    </li>
  <?php endif; ?>

  <?php if ( ! empty( $social_tw )) : ?>
    <li>
      <a target="_blank" href="https://twitter.com/<?php echo $social_tw; ?>">
        <svg class="social-tw-svg" width="31" height="31" focusable="false">
          <use href="#social-twitter"/>
        </svg>
      </a>
    </li>
  <?php endif; ?>

  <?php if ( ! empty( $social_gp )) : ?>
    <li>
      <a target="_blank" href="<?php echo $social_gp; ?>">
        <svg class="social-tw-svg" width="31" height="31" focusable="false">
          <use href="#social-google"/>
        </svg>
      </a>
    </li>
  <?php endif; ?>

  <?php if ( ! empty( $social_yt )) : ?>
    <li>
      <a target="_blank" href="<?php echo $social_yt; ?>">
        <svg class="social-tw-svg" width="31" height="31" focusable="false">
          <use href="#social-youtube"/>
        </svg>
      </a>
    </li>
  <?php endif; ?>
</ul>

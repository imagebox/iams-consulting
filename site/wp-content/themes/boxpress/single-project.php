<?php
/**
 * The template for displaying all single posts.
 *
 * @package BoxPress
 */

get_header(); ?>

	<?php require_once('template-parts/banners/banner--project.php'); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php if ( have_posts() ) : ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<div class="post-navigation-section">
						<div class="wrap">
							<?php
								the_post_navigation( array(
									'prev_text' => 'Previous Project',
									'next_text' => 'Next Project',
									'screen_reader_text' => 'Projects Navigation',
								));
							?>
							<div class="post-navigation-center">
								<?php
									$referer_url = wp_get_referer();
									$back_url = '';

									if ( strpos( $referer_url, '/project/building-type/' ) !== false ||
									     strpos( $referer_url, '/project/design-category/' ) !== false ) {
										$back_url = $referer_url;

									} else {

										$terms_building_type 	= get_the_terms( get_the_ID(), 'project_building_type' );
										$terms_desgin_cat 		= get_the_terms( get_the_ID(), 'project_design_category' );

										if ( $terms_building_type && ! is_wp_error( $terms_building_type )) {
											$back_url = get_term_link( $terms_building_type[0] );

										} elseif ( $terms_desgin_cat && ! is_wp_error( $terms_desgin_cat )) {
											$back_url = get_term_link( $terms_desgin_cat[0] );

										} else {

											$back_url = '/project/building-type/k-12/';
										}
									}
								?>
								<a href="<?php echo esc_url( $back_url ); ?>">Back to Portfolio</a>
							</div>
						</div>
					</div>

					<section class="section">
						<div class="wrap">
							<div class="l-sidebar">
								<div class="entry-content">

									<header class="heading-group">
										<h1><?php the_title(); ?></h1>
										<h4 class="sub-title">Portfolio</h4>
									</header>
									
									<?php the_content(); ?>

								</div>

								<?php get_sidebar(); ?>

							</div>
						</div>
					</section>


					<?php if ( have_rows( 'project_carousel' )) : ?>

						<section class="section">
							<div class="wrap">
								<div class="owl-carousel">

									<?php while ( have_rows( 'project_carousel' )) : the_row();
											$carousel_image = get_sub_field( 'carousel_image' );
										?>
										<?php if ( $carousel_image ) : ?>
											<div class="image-slide">
												<img src="<?php echo $carousel_image['url']; ?>"
													alt="<?php echo $carousel_image['alt']; ?>">
											</div>
										<?php endif; ?>
									<?php endwhile; ?>

								</div>
							</div>
						</section>

					<?php endif; ?>

				<?php endwhile; ?>
			<?php endif; ?>

		</main>
	</div>

<?php get_footer(); ?>
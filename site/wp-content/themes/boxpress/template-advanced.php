<?php 
/**
 * Template Name: Advanced
 */
get_header(); ?>

	<?php require_once('template-parts/banners/banner--page.php'); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php
				/**
				 * Run clean shortcode for repeater sub-fields.
				 * 
				 * Needs to be calld directly before repeaters, won't
				 * work directly in functions.php because of how
				 * sub-fields are called.
				 */
				function boxpress_clean_shortcodes_acf( $content ) {
				  
				  // els to remove
				  $array = array(
				    '<p>['    => '[',
				    ']</p>'   => ']',
				    '<div>['  => '[',
				    ']</div>' => ']',
				    ']<br />' => ']',
				    ']<br>'   => ']',
				    '<br />[' => '[',
				    '<br>['   => '[',
				  );

				  // Remove dem els
				  $content = strtr( $content, $array );
				  return $content;
				}
				add_filter('acf_the_content', 'boxpress_clean_shortcodes_acf');
			?>

			<?php if ( have_rows( 'innerpage_master' )) :
					$row_index = 1;
				?>
				<?php while ( have_rows( 'innerpage_master' )) : the_row(); ?>

					<?php
						// Create layout string
						$layout_path = 'template-layouts/' . str_replace( '_', '-', get_row_layout()) . '.php';

						// Include our layout
						if (( @include $layout_path ) === false ) {
							echo '<div class="boxpress-error"><p>Sorry bud, that layout file is missing!</p></div>';
						}

						$row_index++;
					?>

				<?php endwhile; ?>
			<?php endif; ?>

		</main>
	</div>

<?php get_footer(); ?>

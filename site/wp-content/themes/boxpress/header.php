<!DOCTYPE html>
<html class="no-js" lang="en-US">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>

<?php include_once('template-parts/svg.php'); ?>

<div id="site-wrap">
	<div id="canvas">

		<?php include_once('template-parts/header-mobile.php'); ?>	

		<div id="page" class="hfeed site">

			<header id="masthead" class="site-header">
				<div class="wrap wrap--full">

					<div class="site-header-inner">
						<div class="header-col-1">
		
							<div class="site-branding">
								<a href="<?php echo esc_url( home_url( '/' )); ?>" rel="home">
							    <span class="vh"><?php bloginfo('name'); ?></span>
									<svg class="site-logo" width="60" height="60" focusable="false">
						        <use href="#site-logo" />
									</svg>
								</a>
							</div>

						</div>
						<div class="header-col-2">

							<nav class="main-nav" role="navigation">
								<ul>
									<?php
										wp_nav_menu( array(
									    'theme_location' 	=> 'primary',
									    'items_wrap' 			=> '%3$s',
									    'container' 			=> false,
									  ));
								  ?>
								</ul>
							</nav>

						</div>
					</div>
				</div>
			</header>

			<div id="content" class="site-content">

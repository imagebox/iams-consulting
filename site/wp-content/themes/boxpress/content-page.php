<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package BoxPress
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'boxpress' ),
				'after'  => '</div>',
			) );
		?>

	
</article><!-- #post-## -->

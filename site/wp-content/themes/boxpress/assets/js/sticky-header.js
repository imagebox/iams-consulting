(function ($) {
  'use strict';

  toggleStickyNav();

  $(window).on( 'scroll', function () {
    toggleStickyNav();
  });

  $(window).on( 'resize', function () {
    toggleStickyNav();
  });


  function toggleStickyNav() {
    var $site_header = $('#masthead');

    if ( $site_header.is( ':visible' )) {
      if ( $(window).scrollTop() > 40) {
        $site_header.addClass('minify-header');

      } else {
        $site_header.removeClass('minify-header');
      }
    } else {
      $site_header.removeClass('minify-header');
    }
  }

})(jQuery);

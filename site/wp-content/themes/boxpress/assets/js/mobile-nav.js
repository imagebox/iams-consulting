(function ($) {
	'use strict';

	/****************************************
	* Mobile Navigation
	****************************************/

	// Mobile Navigation Toggle

	var $toggle_nav 		= $('.toggle-nav');
	var $mobile_nav 		= $('.mobile-nav');

	$toggle_nav.on('click', function () {
		toggleNav();
	});

	function toggleNav() {
		$mobile_nav.toggleClass( 'is-active' );

		if ( $mobile_nav.hasClass( 'is-active' )) {
			$mobile_nav.attr( 'aria-hidden', false );
		} else {
			$mobile_nav.attr( 'aria-hidden', true );
		}
	}


	// Delegate the click event to bind the changing ':not(.is-active)' selector
	$('.mobile-nav').on('click', '.menu-item-has-children > a:not(.is-active)', function (e) {
		var $this_link 	= $(this);
		var $sub_menu 	= $this_link.siblings('.sub-menu');

    var $sibling_links = $this_link.closest('li')
    	.siblings('.menu-item-has-children')
    	.find('> a');

		var $sibling_sub_menus = $this_link.closest('li')
			.siblings('.menu-item-has-children')
			.find('> .sub-menu');

		$sibling_links.removeClass('is-active');
		$this_link.addClass('is-active');

		$sibling_sub_menus.slideUp('800');
		$sub_menu.slideDown('800');

		return false;
	});

})(jQuery);

(function ($) {
  'use strict';

  /**
   * Site javascripts
   */

  // Wrap iframes in div w/ flexible-container class to make responsive
  $('iframe:not(.not-responsive)').wrap('<div class="flexible-container"></div>');


  $('.owl-carousel').owlCarousel({
    items: 1,
    loop: true,
    nav: true,
    navText: [
      '<svg class="svg-circle-arrow-left" width="50" height="50" focusable="false"><use href="#circle-arrow-left" /></svg>',
      '<svg class="svg-circle-arrow-right" width="50" height="50" focusable="false"><use href="#circle-arrow-right" /></svg>',
    ],
    responsive: {
      0: {
        nav: false,
      },
      760: {
        nav: true,
      },
    }
  });


  /**
   * Projects Filter
   */
  
  var $project_type_filter = $('#project-type-filter');

  $project_type_filter.on('change', function () {
    var $this = $(this);
    var $this_val = $this.val();

    if ( $this_val != '' ) {
      window.location.href = $this_val;
    }

    return false;
  });


  /**
   * Popups
   */
  MicroModal.init({
    onShow: function ( modal ) {
      var $modal = $( '#' + modal.id );
      $modal.appendTo( 'body' );
    },
    disableScroll: true,
  });


})(jQuery);

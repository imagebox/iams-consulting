<?php
/**
 * The template for displaying the Project Building Type archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package BoxPress
 */

$queried_object = get_queried_object();

get_header(); ?>

  <?php require_once('template-parts/banners/banner--project-archive.php'); ?>

  <div class="filter-bar">
    <div class="wrap">
      <div class="filter-bar-inner">
        <div class="filter-item-left"></div>
        <?php
          $project_cat_links = get_terms(array(
            'taxonomy'    => 'project_design_category',
            'hide_empty'  => true,
          ));
        ?>
        <?php if ( $project_cat_links && ! is_wp_error( $project_cat_links )) : ?>
          <div class="filter-item-right">
            <ul class="filter-design-cats">
              <li><a class="<?php
                  if ( is_post_type_archive( 'project' ) ) {
                    echo 'is-active';
                  }
                ?>" href="<?php echo get_post_type_archive_link( 'project' ); ?>"><?php _e( 'All', 'boxpress' ); ?></a></li>
              <?php foreach ( $project_cat_links as $term ) : ?>
                <li>
                  <a class="<?php
                      if ( isset( $queried_object ) && array_key_exists( 'term_id', $queried_object ) && $queried_object->term_id == $term->term_id ) {
                        echo 'is-active';
                      }
                    ?>" href="<?php echo esc_url( get_term_link( $term )); ?>"><?php echo $term->name; ?></a>
                </li>
              <?php endforeach; ?>
            </ul>
          </div>
        <?php endif; ?>
      </div>

    </div>
  </div>

  <div id="primary" class="content-area">
    <main id="main" class="site-main">

      <section class="section">
        <div class="wrap">
          
          <div class="entry-content">

            <header class="archive-header">
              <h2><?php single_term_title(); ?> <?php _e( 'Buildings', 'boxpress' ); ?></h2>
              <?php the_archive_description( '<div class="taxonomy-description">', '</div>' ); ?>
            </header>

            <?php if ( have_posts() ) : ?>

              <div class="l-grid l-grid--3-col">

                <?php while ( have_posts() ) : the_post(); ?>

                  <div class="l-grid-item">
                    <?php get_template_part( 'content-project-card' ); ?>
                  </div>

                <?php endwhile; ?>
                
              </div>

              <?php boxpress_pagination(); ?>

            <?php else : ?>

              <?php get_template_part( 'content', 'none' ); ?>

            <?php endif; ?>

          </div>

          <?php get_sidebar(); ?>

        </div>
      </section>
    </main>
  </div>

<?php get_footer(); ?>

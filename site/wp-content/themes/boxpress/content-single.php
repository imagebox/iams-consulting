<?php
/**
 * @package BoxPress
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

		<?php if ( has_post_thumbnail() ) : ?>
			<?php the_post_thumbnail('home_index_thumb'); ?>
		<?php endif; ?>

		<div class="entry-meta">
			<?php boxpress_posted_on(); ?>
		</div>
	</header>

	<?php the_content(); ?>

	<footer class="entry-footer">
		<?php include_once('template-parts/social-share.php'); ?>
	</footer>
</article>

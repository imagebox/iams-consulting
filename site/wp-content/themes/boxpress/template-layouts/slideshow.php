<?php
/**
 * Displays the Slideshow layout
 *
 * @package BoxPress
 */

$background 		= get_field('background');
$is_fixed_width = get_field('full_width');
?>
<?php if ( have_rows( 'slides' )) : ?>

	<section class="cycle-outer <?php echo $background; ?>">
		<div class="cycle-wrap <?php
				if ( $is_fixed_width == 'no' ) {
					echo 'fixed-width';
				}
			?>">

		<ul id="cycle-home" class="cycle-slideshow"
			data-cycle-swipe="true"
			data-cycle-swipe-fx="scrollHorz"
			data-cycle-prev=".prev"
			data-cycle-next=".next"
			data-cycle-pause-on-hover="true"
			data-cycle-slides="li"
			data-cycle-fx="scrollHorz"
			data-cycle-timeout="5000">

			<?php while ( have_rows('slides') ) : the_row();
					$is_show_slide 		= get_sub_field( 'show_slide' );
					$content_position = get_sub_field( 'content_position' );
					$image 						= get_sub_field( 'photo' );
					$size 						= 'home_slideshow';
				?>
				<?php if ( $is_show_slide == 'yes' &&
									 $image ) : ?>

					<li>
						<?php echo wp_get_attachment_image( $image, $size ); ?>

						<div class="slide-content <?php echo $content_position; ?>">
							<h1><?php the_sub_field('heading');?></h1>
							<h4><?php the_sub_field('subhead');?></h4>
							<a class="button" href="<?php the_sub_field('link'); ?>">
								<?php the_sub_field('link_text'); ?>
							</a>
						</div>
					</li>

				<?php endif; ?>
			<?php endwhile; ?>
		
		</ul>

		<div class="cycle-controls">
			<a href="#" class="prev">
				<span class="vh">Previous Item</span>
				<svg class="svg-arrow-left-icon" width="10" height="16" focusable="false">
	        <use href="#arrow-left-icon"/>
				</svg>
			</a> 
			<a href="#" class="next">
				<span class="vh">Next Item</span>
				<svg class="svg-arrow-right-icon" width="10" height="16" focusable="false">
	        <use href="#arrow-right-icon"/>
				</svg>
			</a>
		</div>
	</section>

<?php endif; ?>

<?php
/**
 * Displays the Full Width Column layout
 *
 * @package BoxPress
 */

$background = get_sub_field('background');

?>
<section class="fullwidth-column section <?php echo $background; ?>">
  <div class="wrap wrap--limited">
    <div class="column-content">

      <?php the_sub_field('content'); ?>

      <?php if ( get_sub_field('add_a_carousel') == 'yes' ) : ?>
        <?php include( locate_template('template-parts/carousel-block')); ?>
      <?php endif; ?>

    </div>

  </div>
</section>

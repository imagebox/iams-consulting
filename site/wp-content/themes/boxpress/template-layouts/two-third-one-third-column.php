<?php
/**
 * Displays the 2 / 3 Col layout
 *
 * @package BoxPress
 */

$background 			= get_sub_field( 'background' );
$section_heading 	= get_sub_field( 'heading' );
?>
<section class="full-width section <?php echo $background; ?>">
	<div class="wrap">
		
		<?php if ( ! empty( $section_heading )) : ?>

			<div class="section-header">
				<h2><?php echo $section_heading; ?></h2>
			</div>

		<?php endif; ?>
		
		<?php if ( have_rows( 'columns' )) : ?>
			<?php while ( have_rows( 'columns' )) : the_row(); ?>
				
				<div class="columns thirds">

					<?php if ( get_sub_field('column_position') == 'one_left' ) : ?>

						<div class="column-one-third left column">

							<?php the_sub_field('one_third'); ?>

						</div>
						<div class="column-two-thirds column">

							<?php the_sub_field('two_thirds'); ?>

						</div>

					<?php endif; ?>
					<?php if ( get_sub_field('column_position') == 'one_right' ) : ?>

						<div class="column-two-thirds column">

							<?php the_sub_field('two_thirds'); ?>

						</div>
						<div class="column-one-third right column">

							<?php the_sub_field('one_third'); ?>

						</div>

					<?php endif; ?>
			
				</div>

			<?php endwhile; ?>
		<?php endif; ?>

	</div>
</section>
<?php
/**
 * Displays the Large Hero Photo layout
 *
 * @package BoxPress
 */
?>
<section class="hero" style="background-image: url(<?php the_sub_field('photo'); ?>);">
						
	<?php if( get_sub_field('overlay') == 'yes' && get_sub_field('lighter_overlay') == 'no' )  { echo '<div class="overlay-wrap"><div class="overlay"></div>'; }?>
	<?php if( get_sub_field('overlay') == 'yes' && get_sub_field('lighter_overlay') == 'yes' )  { echo '<div class="overlay-wrap"><div class="overlay light"></div>'; }?>
	
	<div class="wrap">
		<div class="hero-speaks">
			<?php if( get_sub_field('overlay_box') == 'yes')  { echo '<div class="inner">'; } ?>
				<?php if(get_sub_field('heading')) { ?>
					<h1><?php the_sub_field('heading');?></h1>
				<?php } ?>
				<?php if(get_sub_field('subhead')) { ?>
					<h3><?php the_sub_field('subhead');?></h3>
				<?php } ?>
				<?php if(get_sub_field('link')) { ?>
					<a class="button red" href="<?php the_sub_field('link');?>"><?php the_sub_field('link_text');?></a>
				<?php } ?>
			<?php if( get_sub_field('overlay_box') == 'yes')  { echo '</div>'; } ?>
		</div>
	</div>

	<?php if( get_sub_field('overlay') == 'yes' ) { echo '</div>'; }?>

</section>

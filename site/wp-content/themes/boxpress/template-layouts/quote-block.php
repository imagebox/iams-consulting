<?php
/**
 * Displays the Quote block
 *
 * @package BoxPress
 */

$background       = get_sub_field( 'background' );
$background_image = get_sub_field( 'quote_background_image' );
$quote_copy       = get_sub_field( 'quote_copy' );
$quote_citation   = get_sub_field( 'quote_citation' );
$job_title        = get_sub_field( 'job_title' );

?>
<section class="fullwidth-column quote-block section <?php echo $background; ?>" <?php if ( ! empty( $background_image )) { echo 'style="background-image:url(' . $background_image['url'] . ');"'; } ?>>
  <div class="wrap wrap--limited">

    <blockquote>

      <?php echo $quote_copy; ?>

      <?php if ( ! empty( $quote_citation )) : ?>

        <cite>
          <?php echo $quote_citation; ?>

          <?php if ( ! empty( $job_title )) : ?>
            <span class="quotee-job-title"><?php echo $job_title; ?></span>
          <?php endif; ?>
        </cite>

      <?php endif ?>

    </blockquote>

  </div>
</section>

<?php
/**
 * Displays the Button Callout layout
 * 
 * @package BoxPress
 */

$background           = get_sub_field('background');
$button_callout_title = get_sub_field('button_callout_title');
?>
<section class="fullwidth-column section-callout-buttons section <?php echo $background; ?>">
  <div class="wrap">

    <div class="callout--buttons">
      
      <h2 class="h1">
        <?php echo $button_callout_title; ?>
      </h2>

      <?php if ( have_rows('button_callout_items') ) : ?>

        <div class="button-group">

          <?php while ( have_rows('button_callout_items') ) : the_row();
              
              // ACF Fields
              $button_title = get_sub_field('button_callout_button_title');
              $button_url   = get_sub_field('button_callout_button_url');
            ?>

            <?php if ( ! empty( $button_title ) &&
                       ! empty( $button_url )) : ?>
  
              <a class="button" href="<?php echo $button_url; ?>">
                <?php echo $button_title; ?>
              </a>

            <?php endif; ?>
          <?php endwhile; ?>

        </div>

      <?php endif; ?>

    </div>

  </div>
</section>
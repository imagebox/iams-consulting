<?php
/**
 * Displays the Parallax Background layout
 *
 * @package BoxPress
 */
?>

<div class="full-width parallax" style="background-image: url(<?php the_sub_field('image'); ?>);"></div>

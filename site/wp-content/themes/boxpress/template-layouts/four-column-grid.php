<?php
/**
 * Displays the Four Column Grid layout
 * 
 * @package BoxPress
 */
$background 			= get_sub_field('background');
$section_heading 	= get_sub_field('heading');
?>
<section class="full-width section <?php echo $background; ?>">
	<div class="wrap">
		
		<?php if ( ! empty( $section_heading )) : ?>
			<div class="section-header">
				<h2><?php echo $section_heading; ?></h2>
			</div>
		<?php endif; ?>
		
		<?php if ( have_rows( 'four_columns' )) : ?>
			<?php while ( have_rows( 'four_columns' )) : the_row(); ?>
		
				<div class="columns four">
					<div class="column-one column">

						<?php the_sub_field('column_one'); ?>

					</div>
					<div class="column-two column">

						<?php the_sub_field('column_two'); ?>

					</div>
					<div class="column-three column">

						<?php the_sub_field('column_three'); ?>

					</div>
					<div class="column-four column">

						<?php the_sub_field('column_four'); ?>

					</div>
				</div>
		
			<?php endwhile; ?>
		<?php endif; ?>

	</div>
</section>

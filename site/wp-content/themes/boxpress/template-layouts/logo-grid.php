<?php
/**
 * Displays the Logo Grid Layout
 *
 * @package BoxPress
 */
?>
<section class="logo-grid fullwidth-column section">
	<div class="wrap">

		<?php the_sub_field('content'); ?>

		<div class="grid">

			<?php if ( have_rows( 'logos' )) : ?>
				<?php while( have_rows( 'logos' )) : the_row(); ?>

					<div class="logo">
						<img src="<?php the_sub_field('logo'); ?>" alt="">
					</div>

				<?php endwhile; ?>
			<?php endif; ?>

		</div>

	</div>
</section>

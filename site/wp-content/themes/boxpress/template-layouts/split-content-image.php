<?php
/**
 * Displays the Slipt Content / Image layout
 *
 * @package BoxPress
 */

$section_heading 	= get_sub_field( 'heading' );
$section_content 	= get_sub_field( 'content' );
$section_image 		= get_sub_field( 'image' );
$image_alignment	= get_sub_field( 'image_alignment' );
?>
<section class="section split-block-section">
	<div class="wrap">

		<div class="split-block split-block--<?php echo $image_alignment; ?>">
	
			<?php if ( ! empty( $section_image )) : ?>

				<div class="image">
					<img src="<?php echo $section_image; ?>" alt="<?php echo $section_heading; ?>">
				</div>

			<?php endif; ?>

			<div class="content">
				<div class="inner">

					<?php if ( ! empty( $section_content )) : ?>

						<?php echo $section_content; ?>

					<?php endif; ?>

				</div>
			</div>
		</div>
	</div>
</section>

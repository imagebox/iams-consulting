<?php
/**
 * Displays the Hero layout
 *
 * @package BoxPress
 */

$hero_subhead 					= get_field('hero_subhead');
$hero_background 				= get_field('background');
$hero_background_image 	= get_field('hero_background_image');
?>
<section class="hero <?php echo $hero_background; ?>" <?php
		if ( $hero_background === 'background-image' &&
		 		 ! empty( $hero_background_image )) {
			echo 'style="background:url(' . $hero_background_image['url'] . ') center no-repeat;background-size:cover;"';
		} ?>>

	<div class="wrap">

		<div class="hero-box">
			
			<h1><?php the_field( 'hero_heading' );?></h1>

			<?php if ( ! empty( $hero_subhead )) : ?>
				<h6><?php the_field( 'hero_subhead' ); ?></h6>
			<?php endif; ?>

			<?php if ( get_field('hero_link')) : ?>
				<a class="button" href="<?php the_field('hero_link'); ?>">
					<?php the_field('hero_link_text'); ?>
				</a>
			<?php endif; ?>
			
		</div>

	</div>
</section>

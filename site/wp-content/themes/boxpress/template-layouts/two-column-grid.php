<?php
/**
 * Displays the Two Col Grid layout
 *
 * @package BoxPress
 */

$background 			= get_sub_field('background');
$section_heading 	= get_sub_field('heading');
?>
<section class="full-width section <?php echo $background; ?>">
	<div class="wrap">

		<?php if ( ! empty( $section_heading )) : ?>

			<div class="section-header">
				<h2><?php echo $section_heading; ?></h2>
			</div>

		<?php endif; ?>

		<?php if ( have_rows( 'two_columns' )) : ?>
			<?php while ( have_rows( 'two_columns' )) : the_row(); ?>

				<div class="columns two">
					<div class="column-one column">

						<?php the_sub_field('column_one'); ?>

					</div>
					<div class="column-two column">

						<?php the_sub_field('column_two'); ?>

					</div>
				</div>

			<?php endwhile; ?>
		<?php endif; ?>

	</div>
</section>

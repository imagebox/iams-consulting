<?php
/**
 * Displays the Blog Posts layout
 * 
 * @package BoxPress
 */

$background 			= get_sub_field('background');
$section_heading 	= get_sub_field('heading');

?>
<section class="blog-posts <?php echo $background; ?>">
	<div class="wrap">

		<?php if ( ! empty( $section_heading )) : ?>

			<div class="section-header">
				<h2><?php echo $section_heading; ?></h2>
			</div>

		<?php endif; ?>

		<ul class="<?php if(get_sub_field('number_of_posts_to_show') == "1") { echo 'posts-one';} ?> <?php if(get_sub_field('number_of_posts_to_show') == "2") { echo 'posts-two';} ?> <?php if(get_sub_field('number_of_posts_to_show') == "3") { echo 'posts-three';} ?> <?php if(get_sub_field('number_of_posts_to_show') == "4") { echo 'posts-four';} ?>">
	
			<?php
				$number_of_posts = get_sub_field('number_of_posts_to_show');
				$blog_query = array( 
					'post_type' => 'post', 
					'posts_per_page' => $number_of_posts,
				);
				$blog_loop = new WP_Query( $blog_query );
				while ( $blog_loop->have_posts() ) : $blog_loop->the_post();
			?>
			<li>
				<?php if(get_sub_field('featured_image') == "yes") { ?>
					<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>">
					<div class="blog-thumb">
						<?php 
							if ( has_post_thumbnail() ) {
								the_post_thumbnail('home_blog_thumb');
							} else { ?>
								<img src="<?php bloginfo('template_directory'); ?>/assets/img/temp/blog.jpg"/>
							<?php }
						?>
					</div>
					</a>
				<?php } ?>
				<article>
					<span class="date"><?php the_time('m.d.y'); ?></span>
					<h1><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>"><?php the_title(); ?></a></h1>
					<?php the_excerpt(); ?>
				</article>
			</li>
			<?php 
				endwhile;
			?>

		</ul>
	</div><!--.wrap-->
</section><!--.blog-posts-->
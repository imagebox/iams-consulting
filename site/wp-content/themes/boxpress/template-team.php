<?php
/**
 * Template Name: Team
 *
 * @package boxpress
 */

get_header(); ?>

  <div id="primary" class="content-area">
    <main id="main" class="site-main">
      <section class="fullwidth-column section">

        <?php
          $team_query = new WP_Query(array(
            'post_type' => 'team',
            'posts_per_page' => -1,
          ));
        ?>
        <?php if ( $team_query->have_posts() ) : ?>
          <div class="wrap">
            <header class="page-header">
              <h1 class="page-title"><?php the_title(); ?></h1>
            </header>
            <div class="l-grid l-grid--3-col">
              <?php while ( $team_query->have_posts() ) : $team_query->the_post(); ?>
                <div class="l-grid-item">
                  <?php get_template_part( 'template-parts/cards/card-team-member' ); ?>
                </div>
              <?php endwhile; ?>
            </div>
          </div>
          <?php wp_reset_postdata(); ?>
        <?php endif; ?>

      </section>
    </main>
  </div>

<?php get_footer(); ?>

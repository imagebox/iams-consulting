<?php
/**
 * Displays the sidebar
 *
 * @package BoxPress
 */
?>
<aside class="sidebar">

	<?php if ( is_singular( 'project' )) :
			$terms_building_type 	= get_the_terms( get_the_ID(), 'project_building_type' );
			$terms_desgin_cat 		= get_the_terms( get_the_ID(), 'project_design_category' );
			$terms_clients 	= get_the_terms( get_the_ID(), 'project_clients' );
			$terms_awards 	= get_the_terms( get_the_ID(), 'project_awards' );
			$terms_location = get_the_terms( get_the_ID(), 'project_location' );
		?>

		<?php if (( $terms_clients && ! is_wp_error( $terms_clients )) ||
							( $terms_desgin_cat && ! is_wp_error( $terms_desgin_cat )) ||
							( $terms_awards && ! is_wp_error( $terms_awards ))) : ?>

			<div class="widget-group">

				<?php if ( $terms_clients && ! is_wp_error( $terms_clients )) : ?>
					<div class="sidebar-widget">
						<h4 class="widget-title">Client</h4>

						<ul class="tax-list">
							<?php foreach (  $terms_clients as $term ) : ?>
								<li><?php echo $term->name; ?></li>
							<?php endforeach; ?>
						</ul>

					</div>
				<?php endif; ?>

				<?php if ( ( $terms_building_type && ! is_wp_error( $terms_building_type )) ||
									 ( $terms_desgin_cat && ! is_wp_error( $terms_desgin_cat ))) : ?>

					<div class="sidebar-widget">
						<h4 class="widget-title">Category</h4>

						<ul class="tax-list">

							<?php if ( $terms_building_type && ! is_wp_error( $terms_building_type )) : ?>
								<?php foreach (  $terms_building_type as $term ) :
										$term_link = get_term_link( $term );
									?>
									<li>
										<a href="<?php echo esc_url( $term_link ); ?>">
											<?php echo $term->name; ?>
										</a>
									</li>
								<?php endforeach; ?>
							<?php endif; ?>

							<?php if ( $terms_desgin_cat && ! is_wp_error( $terms_desgin_cat )) : ?>
								<?php foreach (  $terms_desgin_cat as $term ) :
										$term_link = get_term_link( $term );
									?>
									<li>
										<a href="<?php echo esc_url( $term_link ); ?>">
											<?php echo $term->name; ?>
										</a>
									</li>
								<?php endforeach; ?>
							<?php endif; ?>

						</ul>
					</div>

				<?php endif; ?>

				<?php if ( $terms_awards && ! is_wp_error( $terms_awards )) : ?>
					<div class="sidebar-widget">
						<h4 class="widget-title">Awards</h4>

						<ul class="tax-list">
							<?php foreach (  $terms_awards as $term ) : ?>
								<li><?php echo $term->name; ?></li>
							<?php endforeach; ?>
						</ul>

					</div>
				<?php endif; ?>

				<?php if ( $terms_location && ! is_wp_error( $terms_location )) : ?>
					<div class="sidebar-widget">
						<h4 class="widget-title">Location</h4>

						<ul class="tax-list">
							<?php foreach (  $terms_location as $term ) : ?>
								<li><?php echo $term->name; ?></li>
							<?php endforeach; ?>
						</ul>

					</div>
				<?php endif; ?>

			</div>

		<?php endif; ?>
	<?php endif; ?>


  <?php // Blog Archive + Post ?>
  <?php if ( is_home() ||
             is_category() ||
             is_tag() ||
             is_author() ||
             is_singular( 'post' )) : ?>

    <?php
      /**
       * Blog Category Links
       */
      $blog_cats = wp_list_categories(array(
        'title_li'  => '',
        'echo'      => false,
        'exclude'   => array( 1 ),
      ));
    ?>
    <?php if ( $blog_cats ) : ?>

      <div class="sidebar-widget">
        <h4 class="widget-title"><?php _e('Categories', 'boxpress'); ?></h4>
        <nav class="categories-widget">
          <ul>
            <li class="<?php
                if ( is_home() ) {
                  echo 'current-cat';
                }
              ?>">
              <a href="<?php echo get_permalink( get_option( 'page_for_posts' )); ?>">
                <?php _e( 'All Posts', 'boxpress' ); ?>
              </a>
            </li>
            
            <?php echo $blog_cats; ?>

          </ul>
        </nav>
      </div>

    <?php endif; ?>

  <?php endif; ?>

</aside>
